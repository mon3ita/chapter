# Chapter

## How to test it?

1. Clone the repository

`git clone`

2. Go to repository's folder

`cd path_to_repo`

3. Run `python dependencies.py` for installing all required packages

4. Run `pyton run.py` for running the app

5. Visit `localhost:300n`, where n = 0, 1..., where n depends from other apps started on previous ports

Enjoy :)

