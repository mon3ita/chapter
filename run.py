import os
import threading

def run(name):
    command = ("npm start --prefix ./frontend" if name == "npm" 
                else "python backend/manage.py runserver")
    os.system(command)

threads = []
for name in ["npm", "django"]:
    threads.append(threading.Thread(target=run, args=(name, ), daemon=True))
    threads[-1].start()

for thread in threads:
    thread.join()