import axios from 'axios';

const axios_instance = axios.create({
    baseURL: 'http://localhost:8000/api',
    headers: {
        'Content-Type': 'application/json'
    }
});

function getUser() {
    //fix

    const user = localStorage.getItem("username");
    const token = localStorage.getItem("Token");

    return [user, token];
}


function getDate(date) {
    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

function getSize(arr) {
    const size = arr ? arr.length : 0;
    return size;
}

function changeLocation(newLoc) {
    window.location.href = newLoc;
}

export { axios_instance, getUser, getDate, getSize, changeLocation };