import Header from './components/layout/Header';
import Body from './components/layout/Body';

function App() {
  return (
    <div id="layoutSidenav">
        <Header />
        <Body />
    </div>
  );
}

export default App;
