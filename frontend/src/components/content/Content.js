import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import Chapter from '../chapter/Chapter';

export default class Content extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        let content;
        if(this.props.chapters.length) {
            content = this.props.chapters.map((chapter, index) => 
                <div>
                    <h4><Link to={`books/${this.props.book_id}/${chapter.id}`}>{chapter.title_number}. {chapter.title}</Link></h4>
                </div>
            );
        }
        else {
            content = <h3>No content</h3>;
        }

        return(
            <div>
                {content}
            </div>
        )
    }
}