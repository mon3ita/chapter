import React, { Component } from 'react';

import { axios_instance, getUser, getSize } from '../../utils';

export default class ChapterCreateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            title_number: "",
            content: "",
            errors: []
        }

        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeTitleNumber = this.handleChangeTitleNumber.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleCreateChapter = this.handleCreateChapter.bind(this);
    }

    handleCreateChapter(e) {

        const book_id = this.props.match.params.id;

        const chapter = {
            title: this.state.title,
            title_number: this.state.title_number,
            content: this.state.content
        };

        const { errors } = this.state;

        if(!chapter.title)
            errors.push("Chapter's title must be provided.")
        if(!chapter.title_number)
            errors.push("Chapter's title number must be provided.")
        if(!chapter.content)
            errors.push("Chapter's content must be provided.")

        if(!errors.length) {
            const token = getUser()[1];
                axios_instance.post(`/books/${book_id}/create/`, chapter, {
                headers: {
                  "Authorization": `Token ${token}`
                }
            }).then(response => {
                this.setState({title: "",
                                title_number: "",
                                content: ""});

                window.location.href = `/books/${book_id}/${response.data.id}`;
            }).catch(err => console.log(err));
        } else {
            this.setState({ errors });
        }

        e.preventDefault();
    }

    handleChangeTitleNumber(e) {
        this.setState({title_number: e.target.value});
    }

    handleChangeTitle(e) {
        this.setState({title: e.target.value});
    }

    handleChangeContent(e) {
        this.setState({content: e.target.value});
    }

    render() {
        const { title, title_number, content } = this.state;

        const errors = this.state.errors.map((error, id) => 
            <div class="alert alert-danger" role="alert">
              {error}
            </div>);

        return(
            <form style={{padding: "10px"}}>
                {errors}
                <div className="form-group">
                <h1>
                    <div className="row">
                        <div className="col col-md-2">
                            <input type="text" placeholder="Title #" className="form-control" value={title_number} onChange={(e) => this.handleChangeTitleNumber(e) } /> 
                        </div>.
                        <div className="col">
                            <input type="text" placeholder="Title" className="form-control" value={title} onChange={(e) => this.handleChangeTitle(e) } />
                        </div>
                    </div>
                </h1>
                </div>
                <hr/>
                <div className="form-group">
                <textarea className="form-control" placeholder="Content" value={content} onChange={(e) => this.handleChangeContent(e) } rows="10"></textarea>
                </div>
                <button className="btn btn-success" onClick={(e) => this.handleCreateChapter(e) }>Create</button>
            </form>
        )
    }
}