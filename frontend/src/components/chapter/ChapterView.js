import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser, getDate } from "../../utils";


export default class Chapters extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const chapter = this.props.chapter;
        const book_id = this.props.book_id;

        console.log(chapter);
        return(
            <div>
                <p className="card mb-12">
                        <div className="card-body">
                            <h2 className="card-title"><Link to={`/books/${book_id}/${chapter.id}`}>{chapter.title_number}. {chapter.title}</Link></h2>
                            <p class="card-text">{chapter.content}</p>
                        </div>
                        <div className="card-footer text-muted">
                                Started at {getDate(new Date(chapter.created_at))}
                                <Link className="btn btn-success btn-sm" style={{marginLeft: '60%'}} title="Write to chapter" to={`/books/${book_id}/${chapter.id}/update`}>+</Link>
                                <button className="btn btn-warning btn-sm" style={{marginLeft: '1px'}} title="Export as PDF">PDF</button>
                                <Link className="btn btn-primary btn-sm" style={{marginLeft: '1px'}} title="Edit chapter" to={`/books/${book_id}/${chapter.id}/update`}>Edit</Link>
                                <button className="btn btn-danger btn-sm" style={{marginLeft: '1px'}} title="Delete chapter" onClick={() => this.props.deleteChapter(chapter.id)}>X</button>
                        </div>
                    </p>
            </div>
        )
    }
}