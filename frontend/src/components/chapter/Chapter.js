import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { axios_instance, getUser } from "../../utils";

import ChapterUpdateForm from './ChapterUpdateForm';


export default class Chapter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chapter: {},
            showUpdateForm: false
        }

        this.handleShowUpdateForm = this.handleShowUpdateForm.bind(this);
        this.handleUpdatechapter = this.handleUpdatechapter.bind(this);
    }

    componentDidMount() {
        const book_id = this.props.match.params.id;
        const chapter_id = this.props.match.params.chapter;

        const [user, token] = getUser();

        axios_instance.get(`/books/${book_id}/${chapter_id}/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({
                chapter: response.data
            });
        }).catch(err => 
            console.log(err)
        );
    }

    handleShowUpdateForm() {
        this.setState({ showUpdateForm: !this.state.showUpdateForm});
    }

    handleUpdatechapter(chapter) {
        const { id, created_at, book } = this.state.chapter;
        let new_chapter = chapter;
        new_chapter["id"] = id;
        new_chapter["created_at"] = created_at;
        new_chapter["book"] = book;
        this.setState({chapter: new_chapter});
    }

    render() {
        const chapter = this.state.chapter;
        const book_id = this.props.match.params.id;

        return(
            <div>
                <hr/>
                <Link className="btn btn-success btn-sm" title="Add To chapter" to={`/books/${book_id}/${chapter.id}/update`}>+</Link>&nbsp;
                <button className="btn btn-warning btn-sm" title="Export as PDF">PDF</button>&nbsp;
                <Link className="btn btn-primary btn-sm" title="Edit" to={`/books/${book_id}/${chapter.id}/update`}>Edit</Link>&nbsp;
                <button className="btn btn-danger btn-sm" title="Delete">X</button>
                <hr/>
                {this.state.showUpdateForm ? <ChapterUpdateForm chapter={chapter} book_id={book_id} updatechapter={this.handleUpdatechapter} showUpdateForm={this.handleShowUpdateForm } /> : 
                    <div>
                        <h1>{chapter.title_number}. {chapter.title}</h1>
                        <hr/>
                        {chapter.content}
                    </div>
                }
                <hr/>
                <button className="btn btn-primary btn-sm" title="Back to Book's main page" onClick={ () => window.location.href=`/books/${book_id}` }>Back</button>&nbsp;
            </div>
        )
    }
}