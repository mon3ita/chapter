import React, { Component } from 'react';

import { axios_instance, getUser } from "../../utils";

export default class ChapterUpdateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            content: "",
            title_number: "",
            errors: []
        }

        this.handleUpdate = this.handleUpdate.bind(this);

        this.handleUpdateTitle = this.handleUpdateTitle.bind(this);
        this.handleUpdateContent = this.handleUpdateTitle.bind(this);
        this.handleUpdateTitleNumber = this.handleUpdateTitleNumber.bind(this);
    }

    componentDidMount() {

        const book_id = this.props.match.params.id;
        const chapter_id = this.props.match.params.chapter;

        const [user, token] = getUser();

        if(!user)
            window.location.href = "/login"

        axios_instance.get(`/books/${book_id}/${chapter_id}/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({title: response.data.title, 
                           title_number: response.data.title_number,
                           content: response.data.content });
        }).catch(err => 
            console.log(err)
        );
    }

    handleUpdate(e) {
        const book_id = this.props.match.params.id;
        const chapter_id = this.props.match.params.chapter;

        const chapter = {
            title: this.state.title,
            title_number: this.state.title_number,
            content: this.state.content
        }

        const { errors } = this.state;

        if(!chapter.title)
            errors.push("Chapter's title must be provided.")
        if(!chapter.title_number)
            errors.push("Chapter's title number must be provided.")
        if(!chapter.content)
            errors.push("Chapter's content must be provided.")


        if(!errors.length) {
            axios_instance.put(`/books/${book_id}/${chapter_id}/update/`, chapter, {
                headers: {
                    "Authorization": `Token ${getUser()[1]}`
                }
            }).then(response => {
                window.location.href = `/books/${book_id}/${chapter_id}`;
            }).catch(err => 
                console.log(err)
            );
        } else {
            this.setState({ errors });
        }

        e.preventDefault();
    }

    handleUpdateTitle(e) {
        this.setState({ title: e.target.value });
    }

    handleUpdateContent(e) {
        this.setState({ content: e.target.value });
    }

    handleUpdateTitleNumber(e) {
        this.setState({ title_number: e.target.value });
    }

    render() {
        const chapter = this.state;

        const errors = this.state.errors.map((error, id) => 
            <div class="alert alert-danger" role="alert">
              {error}
            </div>);

        return(
            <form>
                {errors}
                <div className="form-group">
                <button onClick={(e) => this.handleUpdate(e) } className="btn btn-success">Update</button>
                <h1>
                    <div className="row">
                        <div className="col col-md-1">
                            <input type="text" className="form-control" value={chapter.title_number} onChange={(e) => this.handleUpdateTitleNumber(e) } /> 
                        </div>.
                        <div className="col">
                            <input type="text" className="form-control" value={chapter.title} onChange={(e) => this.handleUpdateTitle(e) } />
                        </div>
                    </div>
                </h1>
                </div>
                <hr/>
                <div className="form-group">
                <textarea className="form-control" value={chapter.content} onChange={(e) => this.handleUpdateContent(e) } rows="20"></textarea>
                </div>
                <button onClick={(e) => this.handleUpdate(e) } className="btn btn-success">Update</button>
            </form>
        )
    }
}