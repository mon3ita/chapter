import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser, getDate } from "../../utils";

import ChapterView from './ChapterView';

export default class Chapters extends Component {
    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(id) {
        const book_id = this.props.book_id;

        const token = getUser()[1];

        axios_instance.delete(`/books/${book_id}/${id}/delete/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            window.location.href = `/books/${book_id}`;
        }).catch(err => console.log(err));
    }

    render() {
        
        const chapters = this.props.chapters.map((chapter, id) =>
            <ChapterView chapter={chapter}
                            deleteChapter={this.handleDelete}
                            book_id={this.props.book_id}  />
        );

        return(
            <div>
              {chapters}
            </div>
        )
    }
}