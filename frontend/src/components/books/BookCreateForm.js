import React, { Component } from 'react';

import { axios_instance, getUser } from "../../utils";

export default class BookCreateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            prologue: "",
            errors: []
        }

        this.handleCreateBook = this.handleCreateBook.bind(this);
        this.handleTitleOnChange = this.handleTitleOnChange.bind(this);
        this.handlePrologueOnChange = this.handlePrologueOnChange.bind(this);
    }

    handleCreateBook(e) {
      const [user, token] = getUser();

      const { errors } = this.state;
      const book = {
        title: this.state.title,
        prologue: this.state.prologue
      }

      if(!book.title)
        errors.push("Title must be provided.");
      if(book.title.length > 200)
        errors.push("Title's size must be less than 200 characters.");

      if(!book.prologue)
        errors.push("Prologue must be provided");

      if(!errors.length) {
        axios_instance.post("/books/create/", book, {
          headers: {
            "Authorization": `Token ${token}`
          }
        }).then(response => {
          this.setState({
            title: "",
            prologue: ""
          });

          window.location.href = `/books/${response.data.id}`;
        }).catch(err =>
          console.log(err)
        );
      } else {
        this.setState({errors});
      }

      e.preventDefault();
    }

    handleTitleOnChange(e) {
      this.setState({title: e.target.value});
    }

    handlePrologueOnChange(e) {
      this.setState({prologue: e.target.value});
    }

    render() {
    
      const errors = this.state.errors.map((error, id) => 
        <div class="alert alert-danger" role="alert">
          {error}
        </div>);

      return(
            <form style={{marginTop: '5%', marginLeft: '10%'}}>
              {errors}
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Title" onChange={(e) => this.handleTitleOnChange(e) } />
              </div>
              <div className="form-group">
                <textarea className="form-control" placeholder="Prologue" onChange={(e) => this.handlePrologueOnChange(e) }></textarea>
              </div>
              <button type="submit" className="btn btn-success" onClick={(e) => this.handleCreateBook(e) } >Create</button>
              <hr/>
            </form>
      )
    }
}