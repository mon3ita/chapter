import React, { Component } from 'react';

import { axios_instance, getUser, getSize, changeLocation } from "../../utils";


export default class BookUpdateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            prologue: "",
            errors: []
        }

        this.handleBookUpdate = this.handleBookUpdate.bind(this);
    }

    componentDidMount() {

        const book_id = this.props.match.params.id;
        const [user, token] = getUser();

        if(!user)
            window.location.href = "/login"

        axios_instance.get(`/books/${book_id}/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({title: response.data.title, 
                           prologue: response.data.prologue });
        }).catch(err => 
            console.log(err)
        );

    }

    handleBookUpdate(e) {
        const token = getUser()[1];

        const { errors } = this.state;

        const book_id = this.props.match.params.id;

        const book = {
            title: this.state.title,
            prologue: this.state.prologue
        };

        if(!book.title)
            errors.push("Title must be provided.");
        if(book.title.length > 200)
            errors.push("Title's size must be less than 200 characters.");

        if(!book.prologue)
            errors.push("Prologue must be provided");


        if(!errors.length) {
            axios_instance.put(`/books/${book_id}/update/`, book, {
                headers: {
                    "Authorization": `Token ${token}`
                }
            }).then(response => {
                this.setState({title: "",
                                prologue: ""});
                window.location.href = `/books/${book_id}`;
            }).catch(err => {
                console.log(err)
            });
        } else {
            this.setState({ errors });
        }


        e.preventDefault();
    }

    render() {
        const book_id = this.props.match.params.id;

        const errors = this.state.errors.map((error, id) => 
            <div class="alert alert-danger" role="alert">
              {error}
            </div>);

        return(
            <form style={{marginTop: '5%', marginLeft: '10%'}}>
                {errors}
               <button onClick={() => changeLocation(`/books/${book_id}`) } className="btn btn-primary" >Back</button>
                  &nbsp;<button onClick={(e) => this.handleBookUpdate(e)} className="btn btn-success" >Update</button><hr/>
               <div className="form-group">
                  <h2 className="card-title"><input type="text" className="form-control" placeholder="Title" value={this.state.title} onChange={(e) => this.setState({title: e.target.value}) } /></h2>
                  </div>
                  <div className="form-group">
                    <p className="card-text"><textarea rows="30" className="form-control" placeholder="Prologue" value={this.state.prologue} onChange={(e) => this.setState({prologue: e.target.value}) }></textarea></p>
                </div>
              
                <button onClick={(e) => this.handleBookUpdate(e)} className="btn btn-success" >Update</button>
                  <hr/>
            </form>
        )
    }
}
