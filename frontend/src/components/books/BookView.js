import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import Content from '../content/Content';

import { axios_instance, getUser, getSize, changeLocation } from "../../utils";

export default class BookView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showContent: false,
        }
        this.handleShowContent = this.handleShowContent.bind(this);
        this.handleDeleteBook = this.handleDeleteBook.bind(this);
    }

    handleShowContent() {
        this.setState({
            showContent: !this.state.showContent
        });
    }

    handleDeleteBook(id) {
        const token = getUser()[1];

        axios_instance.delete(`/books/${id}/delete/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            changeLocation(window.location.href);
        }).catch(err => console.log(err));
    }


    render() {
        const { showContent } = this.state;

        const book = this.props.book;
        const chapters = getSize(book.chapters);
        
        return(
            <p className="card mb-12">
                <div className="card-body">
                    <div>
                        <h2 className="card-title"><Link to={`books/${book.id}/`}>{book.title}</Link></h2>
                            <p className="card-text">{book.prologue}</p>
                            {
                                showContent ? 
                                        <div>
                                            <button className="btn btn-warning btn-sm" title="Hide Book's Content" onClick={() => this.handleShowContent()}>Hide Content</button>
                                                <hr/>
                                            <Content chapters={book.chapters} book_id={book.id} /> 
                                        </div> :
                                        <button className="btn btn-success btn-sm" title="Show Book's Content" onClick={() => this.handleShowContent()}>Show Content</button>
                            
                    }</div>
                </div>
                <div className="card-footer text-muted">
                    Started at {book.started_at} and has { chapters } chapters
                    <Link className="btn btn-success btn-sm" style={{marginLeft: '48%'}} title="Add Chapter" to={`/books/${book.id}/create`} >+</Link>
                    <Link className="btn btn-warning btn-sm" style={{marginLeft: '1px'}} title="Export as PDF">PDF</Link>
                    <Link className="btn btn-primary btn-sm" style={{marginLeft: '1px'}} title="Edit Book's Basic Information" to={`books/${book.id}/update`} >Edit</Link>
                    <button className="btn btn-danger btn-sm" style={{marginLeft: '1px'}} title="Delete Book" onClick={ () => this.handleDeleteBook(book.id) }>X</button>
                </div>
            </p>
        )
    }
}