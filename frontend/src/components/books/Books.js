import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { axios_instance, getUser, changeLocation } from "../../utils";

import BookView from './BookView';

export default class Books extends Component {
    constructor(props) {
        super(props);

        this.state = {
            books: []
        }
    }

    componentDidMount() {
        const [user, token] = getUser();

        if(!user)
            changeLocation("/login");

        axios_instance.get("/books/", {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({ books: response.data })
        }).catch(err => {
            console.log(err)
        });
    }

    render() {

        const books = this.state.books.map((book, id) => 
            <BookView book={book} 
                        book_idx={id}
            />
        );

        return (
            <div>
                <Link className="btn btn-success btn-sm" to="/books/create/">Create</Link>
                <hr/>
                <h1>Books</h1>
                <hr/>
                {books}
            </div>
        )
    }
}