import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser, getDate } from "../../utils";

import BookUpdateForm from './BookUpdateForm';

import Chapters from '../chapter/Chapters';
import ChapterCreateForm from '../chapter/ChapterCreateForm';

export default class Book extends Component {
    constructor(props) {
        super(props);

        this.state = {
            book: {},
            chapters: []
        }

        this.handleDeleteBook = this.handleDeleteBook.bind(this);
    }

    componentDidMount() {

        const id = this.props.match.params.id;
        const [user, token] = getUser();

        if(!user)
            window.location.href = "/login"

        axios_instance.get(`/books/${id}/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({book: response.data, 
                            chapters: response.data.chapters });
        }).catch(err => 
            console.log(err)
        );
    }

    handleDeleteBook(e) {
        const book_id = this.state.book.id;

        const token = getUser()[1];

        axios_instance.delete(`/books/${book_id}/delete/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            window.location.href = "/books";
        }).catch(err => console.log(err));

        e.preventDefault();
    }


    render() {
        const { book, showChapterCreateForm, chapters } = this.state;

        return (
            <div>
                <Link className="btn btn-success btn-sm" title="Add Chapter" to={`/books/${book.id}/create`} >+</Link>&nbsp;
                <button className="btn btn-warning btn-sm" title="Export as PDF">PDF</button>&nbsp;
                <Link className="btn btn-primary btn-sm" title="Edit" to={`/books/${book.id}/update`} >Edit</Link>&nbsp;
                <button className="btn btn-danger btn-sm" title="Delete" onClick={(e) => this.handleDeleteBook(e) }>X</button>
                <h1>{book.title}</h1>
                <hr/>
                <h6>Created at: {getDate(new Date(book.created_at))}</h6>
                <hr/>
                {book.prologue}
                <hr/>
                <Chapters chapters={chapters} 
                            book_id={book.id} />

            </div>
        )
    }
}