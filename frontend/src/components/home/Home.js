import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { getUser, axios_instance, changeLocation } from '../../utils';

import BookView from '../books/BookView';
import ToDo from '../todo/ToDo';

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            book: {},
            todo: {}
        }
    }

    componentDidMount() {
        const [user, token] = getUser();

        if(!user)
            changeLocation("/login");

        const headers = {
            headers: {
                "Authorization": `Token ${token}`
            }
        };

        axios_instance.get("/books/last/", headers)
            .then((response) => {
                    this.setState({
                        book: response.data
                    });
            }).catch(err =>
                console.log(err)
        );

        axios_instance.get("/todos/last/", headers)
            .then((response) => {
                this.setState({
                    todo: response.data
                });
            })
            .catch(err =>
                console.log(err)
        );
    }

    render() {
        const book = this.state.book;
        const todo = this.state.todo;

        const show_book = book.title ? <BookView book={book} /> : <div><h4><i>No books</i></h4></div>;
        const show_todo = todo.title ? <ToDo todo={todo} /> : <div><h4><i>No todos</i></h4></div>;

        return(
            <div>
                <h3>Last Book <Link to="/books" title="To All Books"><i className="fa fa-mail-forward"></i></Link></h3>
                <hr/>
                {show_book}

                <hr/><br/><br/>
                <h3>Last ToDo <Link to="/todos" title="To All Todos"><i className="fa fa-mail-forward"></i></Link></h3>
                <hr/>
                {show_todo}
            </div>
        )
    }
}