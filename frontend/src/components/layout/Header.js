import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { getUser } from '../../utils';

export default class Header extends Component {

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout() {
        localStorage.removeItem("username");
        localStorage.removeItem("Token");
        window.location.href = "/login";
    }

    render() {

        const user = getUser()[0];

        return (
            <div className="content">
                <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                    <Link className="navbar-brand" to="/">CH<i className="fa fa-book"></i>PTER</Link>
                    <Link className="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" to="#"><i className="fas fa-bars"></i></Link>
                    <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                        <div className="input-group">
                            <input className="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button"><i className="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                    <ul className="navbar-nav ml-auto ml-md-0">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fas fa-user fa-fw"></i></a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                {user ? <button className="dropdown-item" onClick={this.logout} >Logout</button> :
                                     <span><Link className="dropdown-item" to="/login">Sign In</Link>
                                        <Link className="dropdown-item" to="/register">Sign Up</Link>
                                    </span>}
                                
                            </div>
                        </li>
                    </ul>
                </nav>
                <div id="layoutSidenav_nav">
                    <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div className="sb-sidenav-menu">
                            <div className="nav">
                                <div className="sb-sidenav-menu-heading">Core</div>
                                <Link className="nav-link" to="/">
                                    <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                    Dashboard
                                </Link>
                                <div className="sb-sidenav-menu-heading">Interface</div>
                                <Link className="nav-link collapsed" to="/books" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                                    My Books
                                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </Link>
                                <div className="sb-sidenav-menu-heading">Ideas</div>
                                <Link className="nav-link" to="/todos">
                                    <div className="sb-nav-link-icon"><i className="fas fa-bars"></i></div>
                                    To Do
                                </Link>
                                </div>
                        </div>
                        <div className="sb-sidenav-footer">
                            <div className="small">Logged in as:</div>
                            {user ? user : "Anonymous"}
                        </div>
                    </nav>
                </div>
            </div>
        )
    }
}