import React, { Component } from 'react';

import { Switch, Route } from 'react-router-dom';

import Home from '../home/Home';

import SignIn from '../users/SignIn';
import SignUp from '../users/SignUp';

import Books from '../books/Books';
import Book from '../books/Book';
import BookUpdateForm from '../books/BookUpdateForm';
import BookCreateForm from '../books/BookCreateForm';

import Chapters from '../chapter/Chapters';
import ChapterCreateForm from '../chapter/ChapterCreateForm';
import ChapterUpdateForm from '../chapter/ChapterUpdateForm';
import Chapter from '../chapter/Chapter';

import ToDos from '../todo/ToDos';
import ToDoUpdateForm from '../todo/ToDoUpdateForm';
import ToDoCreateForm from '../todo/ToDoCreateForm';
import ToDo from '../todo/ToDo';

export default class Body extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div id="layoutSidenav_content" style={{margin: '5%'}}>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login/" component={SignIn} />
                    <Route exact path="/register" component={SignUp} />
                    <Route exact path="/books/" component={Books} />
                    <Route exact path="/books/create/" component={BookCreateForm} />
                            
                    <Route exact path="/books/:id/" component={Book} />
                    <Route exact path="/books/:id/update" component={BookUpdateForm} />
                    <Route exact path="/books/:id/create" component={ChapterCreateForm} />
                    <Route exact path="/books/:id/:chapter/update" component={ChapterUpdateForm} />
                    <Route exact path="/books/:id/:chapter" component={Chapter} />
                    <Route exact path="/todos" component={ToDos} />
                    <Route exact path="/todos/create" component={ToDoCreateForm} />
                    <Route exact path="/todos/:id" component={ToDo} />
                    <Route exact path="/todos/:id/update" component={ToDoUpdateForm} />
                </Switch>
            </div>
        )
    }
}