import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
          username: "",
          password: ""
        }

        this.login = this.login.bind(this);
    }

    login(e) {
      axios_instance.post("/users/login/", {
        username: this.state.username,
        password: this.state.password
      }).then(response => {
          localStorage.setItem("Token", response.data.token);
          localStorage.setItem("username", this.state.username);

          this.setState({
            username: "",
            password: ""
          });

          window.location.href = "/";
        }
      ).catch(err => 
        console.log(err)
      );

      e.preventDefault();
    }

    render() {
        return(
            <form style={{marginTop: '10%', marginLeft: '10%'}}>
              <h1>Login</h1>
              <hr/>
              <div className="form-group">
                <label for="username">Username</label>
                <input type="text" className="form-control" placeholder="Enter username" onChange={(e) => this.setState({username: e.target.value})} />
              </div>
              <div className="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" className="form-control" placeholder="Password" onChange={(e) => this.setState({password: e.target.value})} />
              </div>
              <button type="submit" className="btn btn-primary" onClick={(e) => this.login(e)}>Sign In</button>
            </form>
        )
    }
}