import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
          username: "",
          password: "",
          password2: "",
          errors: []
        }

        this.handleSignUp = this.handleSignUp.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePasswordAgainChange = this.handlePasswordAgainChange.bind(this);
    }

    handleSignUp(e) {

      const errors = [];

      const user = {
        username: this.state.username,
        password: this.state.password,
        password2: this.state.password2
      };

      if(user.password2 != user.password)
        errors.push("Passwords must match.")

      if(!user.username)
        errors.push("Username must be provided.")

      if(!user.password)
        errors.push("Password must be provided.")

      if(user.password.length < 10)
        errors.push("Password must be at least 10 chars long.")

      if(!errors.length) {
        axios_instance.post("/users/create/", user)
          .then(response => {
            window.location.href = "/login";
          }).catch(err => console.log(err));
      }

      this.setState({ errors });
      e.preventDefault();
    }

    handleUsernameChange(e) {
      this.setState({username: e.target.value});
    }

    handlePasswordChange(e) {
      this.setState({password: e.target.value});
    }

    handlePasswordAgainChange(e) {
      this.setState({password2: e.target.value});
    }

    render() {
      const errors = this.state.errors.map((error, id) => 
        <div class="alert alert-danger" role="alert">
          {error}
        </div>);

        return(
            <form style={{marginTop: '10%', marginLeft: '10%'}}>
              <h1>Registration</h1>
              <hr/>
              {errors}
              <br/>
              <div className="form-group">
                <label>Username</label>
                <input type="text" className="form-control" placeholder="Enter username" onChange={(e) => this.handleUsernameChange(e) } />
              </div>
              <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control"  placeholder="Password" onChange={(e) => this.handlePasswordChange(e) } />
              </div>
              <div className="form-group">
                <label>Password Again</label>
                <input type="password" className="form-control" placeholder="Password Again" onChange={(e) => this.handlePasswordAgainChange(e) }/>
              </div>
              <button type="submit" className="btn btn-success" onClick={(e) => this.handleSignUp(e) }>Sign Up</button>
            </form>
        )
    }
}