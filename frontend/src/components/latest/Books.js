import React, { Component } from 'react';

export default class LatestBooks extends Component {
    constructor(props) {
        super(props);

        this.state = {
            books: [{
                title: 'Name',
                chapters: 20,
                description: 'Some basic description',
                last_update: '10/10/2020',
                content: [
                    '1. Introduction, page 1-20',
                    '2. Examples'
                ],
                showContent: false
            }, {
                title: 'Name',
                chapters: 20,
                description: 'Some basic description',
                last_update: '10/10/2020',
                content: [
                    '1. Introduction, page 1-20',
                    '2. Examples'
                ],
                showContent: false
            }]
        }
    }

    render() {

        return(
            <div>
                Books
            </div>
        )
    }
}