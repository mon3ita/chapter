import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { axios_instance, getUser, getDate } from '../../utils';

export default class ToDo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todo: this.props.todo
        };

        this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
    }

    handleDeleteTodo() {
        const token = getUser()[1];
        const id = this.state.todo.id;

        axios_instance.delete(`/todos/${id}/delete/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            window.location.href = "/"
        }).catch(err => console.log(err));
    }

    handleChangeStatus() {
        const token = getUser()[1];
        const todo = this.state.todo;

        todo.status = !todo.status;

        axios_instance.put(`/todos/${todo.id}/update/`, todo, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            window.location.href = "/todos"
        }).catch(err => console.log(err)); 
    }

    render() {

        const todo = this.state.todo;
        const status = this.state.todo.status;

        return(
            <p className="card mb-12">
                <div className="card-body">
                    <h2 className="card-title">{todo.title}</h2>
                    <p class="card-text">{todo.description}</p>
                </div>
                <div className="card-footer text-muted">
                    Created at {getDate( new Date(todo.created_at) ) }
                    <button className={status ? "btn btn-primary btn-sm" : "btn btn-success btn-sm"} style={{marginLeft: '65%'}} title="Done" onClick={ this.handleChangeStatus } >Done</button>
                    <Link className="btn btn-primary btn-sm" style={{marginLeft: '1px'}} title="Edit ToDo" to={`/todos/${todo.id}/update`} >Edit</Link>
                    <button className="btn btn-danger btn-sm" style={{marginLeft: '1px'}} title="Delete ToDo" onClick={ this.handleDeleteTodo }>X</button>
                </div>
            </p>
        )
    }
}