import React, { Component } from 'react';

import { axios_instance, getUser } from "../../utils";

export default class ToDoUpdateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
          title: "",
          description: ""
        }

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleToDoUpdate = this.handleToDoUpdate.bind(this);
    }

    componentDidMount() {

      const id = this.props.match.params.id;
      const token = getUser()[1];

      axios_instance(`/todos/${id}/`, {
        headers: {
          "Authorization": `Token ${token}`
        }
      }).then(response => {
        this.setState({
          title: response.data.title,
          description: response.data.description
        });
      }).catch(err => console.log(err));
    }

    handleTitleChange(e) {
      this.setState({
        title: e.target.value
      });
    }

    handleDescriptionChange(e) {
      this.setState({
        description: e.target.value
      });
    }

    handleToDoUpdate(e) {
      const id = this.props.match.params.id;
      const token = getUser()[1];

      const todo = this.state;

      axios_instance.put(`/todos/${id}/update/`, todo, {
        headers: {
          "Authorization": `Token ${token}`
        }
      }).then(response => {
        window.location.href = `/todos/`;
      }).catch(err => console.log(err));

      e.preventDefault();
    }

    render() {
        return(
            <form style={{marginTop: '5%', marginLeft: '10%'}}>
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Title" value={this.state.title} onChange={(e) => this.handleTitleChange(e) } />
              </div>
              <div className="form-group">
                <textarea className="form-control" placeholder="Description" value={this.state.description} onChange={(e) => this.handleDescriptionChange(e) } />
              </div>
              <button className="btn btn-warning" onClick={(e) => this.handleToDoUpdate(e) }>Update</button>
              <hr/>
            </form>
        )
    }
}