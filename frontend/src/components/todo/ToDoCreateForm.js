import React, { Component } from 'react';

import { axios_instance, getUser } from '../../utils';

export default class ToDoCreateForm extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            description: ""
        };

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleCreateToDo = this.handleCreateToDo.bind(this);
    }

    handleCreateToDo(e) {

      const token = getUser()[1];
      const todo = this.state;

      axios_instance.post(`/todos/create/`, todo, {
        headers: {
          "Authorization": `Token ${token}`
        }
      }).then(response => {
        window.location.href = "/todos";
      }).catch(err => console.log(err));

      e.preventDefault();
    }

    handleTitleChange(e) {
      this.setState({title: e.target.value });
    }

    handleDescriptionChange(e) {
      this.setState({ description: e.target.value });
    }

    render() {
      const user = getUser()[0];

      if(!user)
        window.location.href = '/login';

      return(
            <form style={{marginTop: '5%', marginLeft: '10%'}}>
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Title" onChange={(e) => this.handleTitleChange(e) } />
              </div>
              <div className="form-group">
                <textarea className="form-control" placeholder="Description" onChange={(e) => this.handleDescriptionChange(e) } />
              </div>
              <button type="submit" className="btn btn-success" onClick={(e) => { this.handleCreateToDo(e) }} >Create</button>
              <hr/>
            </form>
      )
    }
}