import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser } from "../../utils";

import ToDo from "./ToDo";

export default class ToDos extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todos: [],
        }
    }

    componentDidMount() {

        const [user, token] = getUser();

        if(!user)
            window.location.href = "/login";

        axios_instance.get(`/todos/`, {
            headers: {
                "Authorization": `Token ${token}`
            }
        }).then(response => {
            this.setState({todos: response.data});
        }).catch(err => console.log(err));
    }

    render() {
        const todos = this.state.todos.map((todo, id) =>
                <ToDo todo={todo} />
        );

        return(
            <div>
                <Link className="btn btn-success btn-sm" to="/todos/create">Create</Link>
                <hr/>
                <h1>To Dos</h1>
                <hr/>
                {todos}
            </div>
        )
    }
}