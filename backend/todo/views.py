from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import ToDo
from .serializers import ToDoSerializer


class ListTodos(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, response="json"):
        todos = ToDoSerializer(self.request.user.todos, many=True)
        return Response(todos.data, status=status.HTTP_202_ACCEPTED)


class GetTodo(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, todo_id, response="json"):
        todo = ToDo.objects.get(pk=todo_id, user=request.user)
        todo = ToDoSerializer(todo)
        return Response(todo.data, status=status.HTTP_202_ACCEPTED)


class GetLastUpdatedTodo(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, response="json"):
        todo = ToDo.objects.filter(user=request.user).latest("updated_at")
        serialized = ToDoSerializer(todo)
        return Response(serialized.data, status=status.HTTP_202_ACCEPTED)


class CreateTodo(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "post"]

    def post(self, request, response="json"):
        request.data["user"] = request.user
        todo = ToDo.objects.create(**request.data)
        todo.save()

        todo = ToDoSerializer(todo)
        return Response(todo.data, status=status.HTTP_202_ACCEPTED)


class UpdateTodo(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "put"]

    def put(self, request, todo_id, response="json"):
        title = request.data["title"]
        description = request.data["description"]

        try:
            todo_status = request.data["status"]
            todo = ToDo.objects.filter(pk=todo_id).update(status=todo_status)
        except:
            todo = ToDo.objects.filter(pk=todo_id).update(
                title=title, description=description
            )

        if todo:
            return Response(request.data, status=status.HTTP_202_ACCEPTED)


class DeleteTodo(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "delete"]

    def delete(self, request, todo_id, response="json"):
        deleted = ToDo.objects.filter(pk=todo_id).delete()
        if deleted:
            return Response({}, status=status.HTTP_202_ACCEPTED)
