from django.urls import path
from . import views

urlpatterns = [
    path("", views.ListTodos.as_view(), name="todos"),
    path("last/", views.GetLastUpdatedTodo.as_view(), name="last_todo"),
    path("<int:todo_id>/", views.GetTodo.as_view(), name="todo"),
    path("<int:todo_id>/update/", views.UpdateTodo.as_view(), name="update_todo"),
    path("create/", views.CreateTodo.as_view(), name="create_todo"),
    path("<int:todo_id>/delete/", views.DeleteTodo.as_view(), name="delete_todo"),
]
