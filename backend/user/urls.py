from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path("token/obtain/", jwt_views.TokenObtainPairView.as_view(), name="token_create"),
    path("token/refresh/", jwt_views.TokenRefreshView.as_view(), name="token_refresh"),
    path("create/", views.CreateUserView.as_view(), name="create_user"),
    path("login/", views.LoginUserView.as_view(), name="login_user"),
    path("user/", views.UserView.as_view(), name="get_user"),
]
