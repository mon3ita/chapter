from rest_framework import status, permissions, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import UserSerializer, RegistrationUserSerializer, LoginUserSerializer

from knox.models import AuthToken


class CreateUserView(APIView):
    permission_classes = (permissions.AllowAny,)
    http_method_names = ["post", "head"]

    def post(self, request, format="json"):
        serializer = RegistrationUserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginUserView(APIView):
    permission_classes = (permissions.AllowAny,)
    http_method_names = ["post", "head", "get"]

    def post(self, request, format="json"):
        serializer = LoginUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = AuthToken.objects.create(user)

        return Response(
            {
                "user": UserSerializer(serializer.validated_data).data,
                "token": token[1],
            },
            status=status.HTTP_202_ACCEPTED,
            headers={
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        )


class UserView(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
