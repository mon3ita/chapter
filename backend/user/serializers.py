from django.contrib.auth import authenticate

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User

from book.serializers import BookSerializer


class RegistrationUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(min_length=10)

    class Meta:
        model = User
        fields = ("id", "username", "password")

    def create(self, data):
        user = User.objects.create_user(data["username"], "", data["password"])
        return user


class LoginUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def create(self, data):
        user = authenticate(**data)

        if user and user.is_active:
            return user
        return serializers.ValidationError("Given username or password is not valid.")


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "password")
