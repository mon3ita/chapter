from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import BookSerializer, ChapterSerializer
from .models import Book, Chapter


class ListBooks(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, response="json"):
        books = BookSerializer(self.request.user.books, many=True)
        return Response(books.data, status=status.HTTP_202_ACCEPTED)


class GetBook(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, book_id, response="json"):
        book = Book.objects.get(pk=book_id, author_id=request.user.id)
        serialized = BookSerializer(book)
        return Response(serialized.data, status=status.HTTP_202_ACCEPTED)


class GetLastUpdatedBook(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get"]

    def get(self, request, response="json"):
        book = Book.objects.filter(author=request.user).latest("updated_at")
        serialized = BookSerializer(book)
        return Response(serialized.data, status=status.HTTP_202_ACCEPTED)


class CreateBook(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "post"]

    def post(self, request, response="json"):
        request.data["author"] = self.request.user
        book = Book.objects.create(**request.data)
        book.save()

        if book:
            book = BookSerializer(book)
            return Response(book.data, status=status.HTTP_202_ACCEPTED)


class UpdateBook(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "put", "post"]

    def put(self, request, book_id, response="json"):
        title, prologue = request.data["title"], request.data["prologue"]

        book = Book.objects.filter(pk=book_id).update(title=title, prologue=prologue)

        if book:
            return Response(request.data, status=status.HTTP_202_ACCEPTED)


class DeleteBook(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "delete"]

    def delete(self, request, book_id, response="json"):
        deleted = Book.objects.filter(pk=book_id).delete()
        if deleted:
            return Response({}, status=status.HTTP_202_ACCEPTED)


class GetContent(APIView):
    permission_classes = (permissions.IsAuthenticated,)


class CreateChapter(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, book_id, response="json"):
        request.data["book"] = book_id
        chapter = ChapterSerializer(data=request.data)
        chapter.is_valid(raise_exception=True)
        chapter.save()
        return Response(chapter.data, status=status.HTTP_202_ACCEPTED)


class GetChapter(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, book_id, chapter_id, response="json"):
        chapter = Chapter.objects.get(book_id=book_id, pk=chapter_id)
        serialized = ChapterSerializer(chapter)
        return Response(serialized.data, status=status.HTTP_202_ACCEPTED)


class UpdateChapter(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "get", "put"]

    def put(self, request, book_id, chapter_id, response="json"):
        chapter = Chapter.objects.filter(pk=chapter_id).update(
            title_number=request.data["title_number"],
            title=request.data["title"],
            content=request.data["content"],
        )
        if chapter:
            return Response(request.data, status=status.HTTP_202_ACCEPTED)

        return Response({}, status=status.HTTP_202_ACCEPTED)


class DeleteChapter(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ["head", "delete"]

    def delete(self, request, book_id, chapter_id, response="json"):
        deleted = Chapter.objects.filter(pk=chapter_id).delete()
        if deleted:
            return Response({}, status=status.HTTP_202_ACCEPTED)
