from django.urls import path
from . import views

urlpatterns = [
    path("", views.ListBooks.as_view(), name="books"),
    path("last/", views.GetLastUpdatedBook.as_view(), name="last_book"),
    path("create/", views.CreateBook.as_view(), name="create_book"),
    path("<int:book_id>/", views.GetBook.as_view(), name="get_book"),
    path("<int:book_id>/update/", views.UpdateBook.as_view(), name="update_book"),
    path("<int:book_id>/delete/", views.DeleteBook.as_view(), name="delete_book"),
    path(
        "<int:book_id>/<int:chapter_id>/",
        views.GetChapter.as_view(),
        name="get_chapter",
    ),
    path(
        "<int:book_id>/<int:chapter_id>/update/",
        views.UpdateChapter.as_view(),
        name="update_chapter",
    ),
    path("<int:book_id>/create/", views.CreateChapter.as_view(), name="create_chapter"),
    path(
        "<int:book_id>/<int:chapter_id>/delete/",
        views.DeleteChapter.as_view(),
        name="delete_chapter",
    ),
]
