from rest_framework import serializers

from .models import Book, Chapter


class ChapterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chapter
        fields = "__all__"


class BookSerializer(serializers.ModelSerializer):
    chapters = ChapterSerializer(many=True)

    class Meta:
        model = Book
        fields = "__all__"

    def create(self, data):
        instance = self.Meta.model(**data)
        instance.save()

        return instance
