from django.db import models

from django.contrib.auth.models import User


class Book(models.Model):

    title = models.CharField(max_length=200)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="books")
    prologue = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.title} / {self.author} / {self.created_at }"


class Chapter(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    title_number = models.IntegerField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="chapters")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [
            ("book_id", "title_number"),
        ]

    def __str__(self):
        return f"[{self.title_number}] {self.title} / {self.book.title}"
