# Generated by Django 3.1.3 on 2020-11-12 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("book", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="book",
            name="prolog",
            field=models.TextField(default=""),
            preserve_default=False,
        ),
    ]
